#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
RESET="\033[0m"

PASSED=0
FAILED=0

# Change INPUT_FILE value to test another dictionary
INPUT_FILE="words.txt"
QUERIES_FILE="queries.txt"

MY_DICT_FILE="mydict.bin"
REF_DICT_FILE="refdict.bin"

if [ ! -f TextMiningCompiler ] || [ ! -f TextMiningApp ] || [ ! -f "$INPUT_FILE" ]; then
  echo -e "${RED}Ref binaries and words.txt must be in test/ directory${RESET}"
  exit 1
fi

if [ ! -f "$MY_DICT_FILE" ]; then
  START_TIME=$SECONDS
  printf "Compiling my binary... "
  ../cmake-build-release/TextMiningCompiler "$INPUT_FILE" "$MY_DICT_FILE" &>/dev/null
  echo "$((SECONDS - START_TIME))s"
fi
if [ ! -f "$REF_DICT_FILE" ]; then
  START_TIME=$SECONDS
  printf "Compiling refoutput's binary... "
  ./TextMiningCompiler "$INPUT_FILE" "$REF_DICT_FILE" &>/dev/null
  echo "$((SECONDS - START_TIME))s"
fi

SECONDS=0

echo "Starting test session..."
while read -r line; do
  echo "$line" | ../cmake-build-release/TextMiningApp "$MY_DICT_FILE" 1>myoutput 2>/dev/null
  echo "$line" | ./TextMiningApp "$REF_DICT_FILE" 1>refoutput 2>/dev/null

  diff myoutput refoutput &>/dev/null
  if [ $? -eq 0 ]; then
    echo -e "${GREEN}[PASSED]${RESET} $line"
    PASSED=$((PASSED + 1))
  else
    echo -e "${RED}[FAILED]${RESET} $line"
    FAILED=$((FAILED + 1))
  fi
done < "$QUERIES_FILE"

if [ $FAILED -eq 0 ]; then
  echo -e "${GREEN}${PASSED} passed in ${SECONDS}s${RESET}"
else
  echo -e "${RED}${FAILED} failed, ${PASSED} passed in ${SECONDS}s${RESET}"
fi

rm -r myoutput refoutput
exit 0

# Spell Checker

Spell Checker is as its name suggests a simple and fast spell checker using Damerau-Levenshtein distance written in
C++17. Results are returned in json format.

This project is part of the course TMLN 1 given by [Sylvain Utard](https://github.com/redox) at EPITA.

# Prerequisites

- [Boost 1.72.0](https://www.boost.org/users/history/version_1_72_0.html)
- [CMake](https://cmake.org/download/)

## Usage

To use Spell Checker, you will need an input file of words (word frequency).

Here is a small example:

```shell script
$ mkdir cmake-build-release
$ cd cmake-build-release
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make

$ ./TextMiningCompiler /path/to/words.txt /path/to/dict.bin
$ echo "approx 0 test" | ./TextMiningApp /path/to/dict.bin
$ echo "approx 1 test" | ./TextMiningApp /path/to/dict.bin
$ echo "approx 2 test" | ./TextMiningApp /path/to/dict.bin
$ echo "approx 0 test\napprox 1 test\napprox 2 test\napprox 3 test\napprox 4 test" | ./TextMiningApp /path/to/dict.bin
$ cat test.txt | ./TextMiningApp /path/to/dict.bin
```

To run the testsuite, you must place `TextMiningCompiler` and `TextMiningApp` ref binaries and a `words.txt` in `test/`
directory. And then:

```shell script
$ mkdir cmake-build-release
$ cd cmake-build-release
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make check
```

To generate Doxygen documentation, go to any build directory and then `make doc`.

## Questions

1. Décrivez les choix de design de votre programme

    > **TextMiningCompiler** : ce programme charge d'abord les mots du dictionnaire dans un **Trie** puis finalement
    écrit cette structure sur fichier via un dfs suffixe.

    > **TextMiningApp** : ce programme a une approche récursive et "descend" dans notre Trie en testant toutes les
    opérations tant que la distance actuelle est inférieure à la distance maximale. La compléxité est donc plus
    intéressante qu'une approche classique.

2. Listez l’ensemble des tests effectués sur votre programme (en plus des units tests)

    > Toutes les query testées sont dans le fichier `test/queries.txt`. Un example d'utilisation de la testsuite est
    disponible dans la section **Usage**.

    > 4 tests de syntax\
    17 tests de distance 0\
    17 tests de distance 1\
    15 tests de distance 2\
    5 tests de distance 3\
    2 test de distance 4


3. Avez-vous détecté des cas où la correction par distance ne fonctionnait pas (même avec une distance élevée) ?

    > Non.

4. Quelle est la structure de données que vous avez implémentée dans votre projet, pourquoi ?

    > On a implémenté un Trie que l'on compile sur disque.

5. Proposez un réglage automatique de la distance pour un programme qui prend juste une chaîne de caractères en entrée,
donner le processus d’évaluation ainsi que les résultats.

    > On définit un nombre n de suggestions de mots voulus pour notre application. On commence avec une approximation de
    distance 1 et on augmente cette distance tant qu'on a un nombre de resultats inférieure à n. Si pour une distance
    quelconque, on a plus de résultats que n, on peut prendre les n premiers résultats (les résultats ont été au
    préalable triés par distance et fréquence).

6. Comment comptez vous améliorer les performances de votre programme

    > D'une part, on pourrait implémenter un Patricia Trie pour réduire la taille de notre binaire et réduire le nombre
    de sauts dans la fonction de distance. D'autre part, un bloom filter pourrait nous dire directement si un mot existe
    ou non dans notre dictionnaire pour une recherche exacte à distance 0 et ainsi éviter de parcourir notre Trie.

7. Que manque-t-il à votre correcteur orthographique pour qu’il soit à l’état de l’art ?

    > Outre le Patricia Trie, notre correcteur ortographique ne traite les mots d'une phrase que séparément et
    syntaxiquement et non comme un tout avec son contexte, sa grammaire et sa conjugaison.

## Authors

- Maxime Zhang, <maxime.zhang@epita.fr>
- Briac Malbois le borgne, <briac.malbois-le-borgne@epita.fr>

#pragma once

#include <string>
#include "structs.hh"

class Trie
{
public:
    /**
     * @brief Trie constructor.
     */
    Trie();

    /**
     * @brief Build Trie with file specified by \p path.
     * @param path Path to words.
     */
    void build(const std::string& path);

    /**
     * @brief Compile Trie to binary file specified by \p path.
     * @param path Path to dictionary.
     */
    void compile(const std::string& path) const;

private:
    shared_node_t root_;
};

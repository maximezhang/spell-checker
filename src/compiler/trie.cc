#include <algorithm>
#include <fstream>
#include <stack>
#include "trie.hh"

Trie::Trie()
    : root_(std::make_shared<Node>())
{}

void Trie::build(const std::string& path)
{
    std::ifstream ifs(path);
    if (!ifs)
        throw std::invalid_argument(path + ": No such file or directory");

    std::string word;
    size_t occurrence;
    while (ifs >> word && ifs >> occurrence)
    {
        shared_node_t current = this->root_;
        for (char c : word)
        {
            auto it = std::find_if(current->children_.begin(), current->children_.end(), [&c](const shared_node_t& node) {
                return node->c_ == c;
            });

            if (it != current->children_.end())
                current = *it;
            else
            {
                current->children_.push_back(std::make_shared<Node>());
                current->children_.back()->c_ = c;
                current = current->children_.back();
            }
        }
        current->occurrence_ = occurrence;
        current->word_ = word;
    }
}

static uint32_t serialize(const shared_node_t& node, std::stack<uint32_t>& offsets, std::ofstream& ofs)
{
    for (const shared_node_t& child : node->children_)
        serialize(child, offsets, ofs);

    uint32_t current_offset = ofs.tellp();
    uint8_t nb_children = node->children_.size();

    ofs.write((char *) &node->c_, sizeof(node->c_));                    // character
    ofs.write((char *) &node->occurrence_, sizeof(node->occurrence_));  // occurrence
    if (node->occurrence_ != 0)
    {
        ofs.write(node->word_.c_str(), node->word_.size());
        ofs.put('\0');
    }
    ofs.write((char *) &nb_children, sizeof(nb_children));              // nb children
    for (size_t i = 0; i < node->children_.size(); i++)                 // children's offset
    {
        ofs.write((char *) &offsets.top(), sizeof(offsets.top()));
        offsets.pop();
    }

    offsets.push(current_offset);
    return current_offset;
}

void Trie::compile(const std::string& path) const
{
    std::ofstream ofs(path, std::ofstream::binary);
    if (!ofs)
        throw std::invalid_argument(path + ": No such file or directory");

    // Save some space to store root offset
    uint32_t root_offset;
    ofs.seekp(sizeof(root_offset), std::ofstream::beg);

    std::stack<uint32_t> offsets;
    root_offset = serialize(this->root_, offsets, ofs);

    // Write root offset
    ofs.seekp(0, std::ofstream::beg);
    ofs.write((char *) &root_offset, sizeof(root_offset));
}

#pragma once

#include <memory>
#include <vector>

struct Node
{
    char c_;
    uint32_t occurrence_;
    std::string word_;
    std::vector<std::shared_ptr<Node>> children_;
};

using shared_node_t = std::shared_ptr<Node>;

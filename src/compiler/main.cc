#include <iostream>
#include "compiler/trie.hh"


int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: ./TextMiningCompiler /path/to/words.txt /path/to/dict.bin\n";
        return 1;
    }

    try
    {
        Trie trie = Trie();
        trie.build(argv[1]);
        trie.compile(argv[2]);
    }
    catch (std::exception& e)
    {
        std::cerr << "TextMiningCompiler: " << e.what() << '\n';
        return 1;
    }

    return 0;
}

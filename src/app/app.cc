#include <boost/algorithm/string.hpp>
#include <sys/stat.h>
#include <sys/mman.h>
#include <zconf.h>
#include <fcntl.h>
#include <cstring>
#include <algorithm>
#include "app.hh"
#include "storage.hh"

App::App(const std::string &path) {
    this->fd = open(path.c_str(), O_RDONLY);
    if (this->fd == -1)
        throw std::invalid_argument(path + ": No such file or directory");

    struct stat st{};
    fstat(this->fd, &st);
    this->file_size = st.st_size;

    this->root_file = (char *) mmap(nullptr, this->file_size, PROT_READ, MAP_SHARED, this->fd, 0);
    if (this->root_file == MAP_FAILED) {
        close(this->fd);
        throw std::runtime_error("mmap failed");
    }

    this->root_offset = 0;
    memcpy((char *) &this->root_offset, this->root_file, sizeof(this->root_offset));
}

void App::distance(const std::string& word, size_t cur, uint32_t offset, int distance, int max_dist,
                   std::unordered_map<std::string, Record>& records, char poped, bool wasswapped) const
{
    if (distance > max_dist)
        return;

    bool wasswapped_loc = false;
    if (wasswapped) {
        wasswapped_loc = true;
        wasswapped = false;
    }

    char new_poped = word[cur];
    char *cursorPosition = this->root_file + offset;

    // Character
    char c;
    memcpy((char *) &c, cursorPosition, sizeof(char));
    cursorPosition += sizeof(c);

    // Occurrence
    uint32_t occurrence;
    memcpy((char *) &occurrence, cursorPosition, sizeof(occurrence));
    cursorPosition += sizeof(occurrence);

    if (occurrence != 0)
    {
        char buffer[256] = {0};
        size_t i = 0;
        while (*cursorPosition != 0)
        {
            buffer[i] = *cursorPosition;
            i++;
            cursorPosition++;
        }
        cursorPosition++;

        if (cur == word.size())
        {
            Record rec(buffer, occurrence, distance);

            auto found = records.find(rec.word);
            if (found != records.end() && found->second.distance > rec.distance)
                found->second.distance = rec.distance;
            else
                records.insert({rec.word, rec});
        }
    }

    // Children number
    uint8_t nbChildren;
    memcpy((char *) &nbChildren, cursorPosition, sizeof(nbChildren));
    cursorPosition += sizeof(nbChildren);

    // Deletion
    this->distance(word, cur + 1, offset, distance + 1, max_dist, records, new_poped, wasswapped);

    // Children
    for (uint8_t i = 0; i < nbChildren; i++) {
        uint32_t childOffset;
        memcpy((char *) &childOffset, cursorPosition, sizeof(childOffset));
        cursorPosition += sizeof(childOffset);

        char c_child;
        memcpy((char *) &c_child, this->root_file + childOffset, sizeof(c_child));

        // Swap
        int dist_sub = 0;
        if (c_child == poped && c == new_poped && c_child != c && !wasswapped_loc) {
            dist_sub = 1;
            wasswapped = true;
        }

        // Substitution
        int mdist = (c_child == new_poped) ? 0 : 1;
        this->distance(word, cur + 1, childOffset, distance + mdist - dist_sub, max_dist, records,
                       new_poped, wasswapped);

        // Insertion
        this->distance(word, cur, childOffset, distance + 1, max_dist, records, new_poped, wasswapped);
    }
}

static std::vector<Record> mapToVector(const std::unordered_map<std::string, Record>& map)
{
    std::vector<Record> vec;
    vec.reserve(map.size());
    for (const auto& elt : map)
        vec.push_back(elt.second);
    return vec;
}

void App::process(const std::string& line)
{
    std::vector<std::string> tokens;
    boost::split(tokens, line, boost::is_any_of(" "));

    if (tokens.size() < 3 || tokens[0] != "approx")
        return;

    try
    {
        int max_distance = std::max(std::stoi(tokens[1]), 0);
        std::string word = tokens[2];

        std::unordered_map<std::string, Record> records;
        this->distance(word, 0, this->root_offset, 0, max_distance, records, '\0', false);

        std::vector<Record> records_vec = mapToVector(records);
        std::cout << Storage(records_vec).sort().jsonify() << '\n';
    }
    catch (std::exception&)
    {
        return;
    }
}

App::~App()
{
    munmap(this->root_file, this->file_size);
    close(this->fd);
}

#include <algorithm>
#include "storage.hh"

Storage& Storage::sort() {
    std::sort(this->results.begin(), this->results.end());
    return *this;
}

std::string Storage::jsonify() {
    std::string out("[");

    bool found = false;

    auto make_json = [](const std::string& word, uint32_t freq, uint8_t dist) {
        std::string json_block(R"({"word":")" + word);
        json_block += R"(","freq":)" + std::to_string(freq);
        json_block += R"(,"distance":)" + std::to_string(dist);
        json_block += "}";
        return json_block;
    };

    for (const auto& el : this->results) {
        found = true;
        std::string json = make_json(el.word, el.occurrence, el.distance);
        out += json + ",";
    }

    if (found) //for debug, it causes to print an empty array of no results, is this ref behavior too ?
        out.pop_back(); //remove last ","
    out += "]";

    return out;
}

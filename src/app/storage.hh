#pragma once

#include <utility>
#include <vector>
#include <string>
#include "structs.hh"

class Storage
{
public:
    explicit Storage(std::vector<Record>  record)
        : results(std::move(record))
    {}

    /**
     * @brief Sort results vector.
     */
    Storage& sort();

    /**
     * @brief Convert results to json format.
     * @return Json string.
     */
    std::string jsonify();

private:
    std::vector<Record> results;
};

#pragma once

#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include "structs.hh"

class App
{
public:
    /**
     * @brief Map file specified by \p path in memory.
     * @param path Path to dictionary.
     */
    explicit App(const std::string& path);

    /**
     * @brief Process and approx search word.
     * @param line e.g. `approx 2 toto`.
     */
    void process(const std::string& line);

    /**
     * @brief Loop over binary file and save results to @param records.
     *
     * @param word Word to search.
     * @param cur Current head of word.
     * @param constructed Current constructed word when descending the Trie.
     * @param offset Node offset.
     * @param distance Current distance.
     * @param max_dist Max distance.
     * @param records Vector of records.
     * @param poped Transposition param.
     * @param wasswapped Transposition param.
     *
     * @return Current distance.
     */
    void distance(const std::string& word, size_t cur, uint32_t offset, int distance, int max_dist,
                  std::unordered_map<std::string, Record>& records, char poped, bool wasswapped) const;

    /**
     * Unmap file and free resources.
     */
    ~App();

private:
    int fd;
    char *root_file;
    size_t file_size;
    uint32_t root_offset;
};

#pragma once

#include <string>
#include <cstdint>
#include <iostream>
#include <utility>

struct Record
{
    Record(std::string word, uint32_t occurrence, uint8_t distance)
        : word(std::move(word))
        , occurrence(occurrence)
        , distance(distance)
    {}

    std::string word;
    uint32_t occurrence;
    uint8_t distance;
};

inline bool operator>(const Record& lhs, const Record& rhs){
    if (lhs.word.empty()) return false;//prevent segfault from nowhere
    if (lhs.distance > rhs.distance)
        return true;
    else if (lhs.distance < rhs.distance)
        return false;

    if (lhs.occurrence < rhs.occurrence)
        return true;
    else if (lhs.occurrence > rhs.occurrence)
        return false;

    return !std::lexicographical_compare(lhs.word.begin(), lhs.word.end(), rhs.word.begin(), rhs.word.end());
}

inline bool operator<(const Record& lhs, const Record& rhs){
    return !(lhs > rhs);
}

inline bool operator==(const Record& lhs, const Record& rhs){
    return lhs.distance == rhs.distance &&
           lhs.occurrence == rhs.occurrence &&
           lhs.word == rhs.word;
}

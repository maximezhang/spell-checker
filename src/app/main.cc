#include <iostream>
#include "app.hh"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: ./TextMiningApp /path/to/dict.bin\n";
        return 1;
    }

    try
    {
        App app(argv[1]);

        std::string line;
        while (std::getline(std::cin, line))
            app.process(line);
    }
    catch (std::exception& e)
    {
        std::cerr << "TextMiningApp: " << e.what() << '\n';
        return 1;
    }

    return 0;
}
